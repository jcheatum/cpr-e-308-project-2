CFLAGS = -c -O3 -g

.PHONY: all
all: fine coarse

.PHONY: fine
fine: main.o Bank.o line.o requestqueue.o transaction.o
	gcc $^ -lpthread -o appserver

.PHONY: coarse
coarse: main-coarse.o Bank.o line.o requestqueue.o transaction.o
	gcc $^ -lpthread -o appserver-coarse

%.o: src/%.c
	gcc $(CFLAGS) $<

.PHONY: clean
clean:
	rm -f *.o

.PHONY: tarball
tarball:
	tar -cvf proj2-jcheatum.tar.gz src Makefile