#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <pthread.h>
#include <sys/time.h>

#include "Bank.h"
#include "line.h"
#include "requestqueue.h"
#include "transaction.h"

int num_accounts;

// The file to log responses to
FILE *outfile;
RequestQueue rq;

// true iff the the user has entered the END request.
bool done;

pthread_mutex_t rq_mut,     // Request queue mutex
                acc_mut;    // Mutex for the entire bank

// Condition to signal the worker threads to stop waiting
pthread_cond_t worker_cond; 

void *worker();

int main(int argc, char **argv) {
    if (argc != 4) {
        fprintf(stderr, "Error: Incorrect number of arguments\n");
        fprintf(stderr, "Usage: %s <# of worker threads> <# of accounts> <output file>\n", argv[0]);
        exit(1);
    }

    int num_threads = atoi(argv[1]), next_id = 1;
    num_accounts = atoi(argv[2]);
    pthread_t threads[num_threads];
    done = false;
    outfile = fopen(argv[3], "w");
    if (outfile == NULL) {
        perror("Error");
        exit(1);
    }

    initialize_accounts(num_accounts);
    rq_init(&rq);
    pthread_mutex_init(&rq_mut, NULL);
    pthread_mutex_init(&acc_mut, NULL);
    pthread_cond_init(&worker_cond, NULL);

    int i;
    for (i = 0; i < num_threads; i++) {
        pthread_create(&threads[i], NULL, worker, NULL);
    }

    // loop until END request is received
    while (!done) {
        printf("> ");
        fflush(stdout);
        char *line = fgetline(stdin);
        if (strlen(line) == 0) {
            free(line);
            continue;
        }
        char **command = split(line);
        Request *r = malloc(sizeof(Request));

        pthread_mutex_lock(&rq_mut);
        struct timeval start_time;
        if (strcmp(command[0], "END") == 0) {           // END request
            done = true;
            free(r);
        } else if (strcmp(command[0], "CHECK") == 0) {  // CHECK request
            r->req_id = next_id++;
            r->type = CHECK;
            r->acc_id = atoi(command[1]);
            gettimeofday(&start_time, NULL);
            r->start = start_time;
            rq_enqueue(&rq, r);
            printf("< ID %d\n", r->req_id);
        } else if (strcmp(command[0], "TRANS") == 0) {  // TRANS request
            size_t len = splitlen(command);
            if ((len - 1) % 2 == 0 && (len - 1) / 2 <= MAX_TRANS) {
                r->req_id = next_id++;
                r->type = TRANS;
                r->trans_list = parse_trans_list(command, len);
                gettimeofday(&start_time, NULL);
                r->start = start_time;
                rq_enqueue(&rq, r);
                printf("< ID %d\n", r->req_id);
            } else {
                fprintf(stderr, "< Error: Invalid number of TRANS arguments\n");
            }
        } else {
            fprintf(stderr, "< Error: Invalid request: '%s'\n", command[0]);
        }
        pthread_cond_broadcast(&worker_cond);
        pthread_mutex_unlock(&rq_mut);

        for (i = 0; command[i] != NULL; i++) {
            free(command[i]);
        }
        free(command);
        free(line);
    }

    // Wait for all threads to finish before exiting
    for (i = 0; i < num_threads; i++) {
        pthread_join(threads[i], NULL);
    }
    fclose(outfile);
    rq_free(&rq);
}

void *worker() {
    Request *request;
    struct timeval time;
    while (!done || rq.length > 0) {
        /* Wait for the queue to not be empty and then take the next request
         * from it */
        pthread_mutex_lock(&rq_mut);
        while (rq.length == 0) {
            pthread_cond_wait(&worker_cond, &rq_mut);

            // Exit once all requests are processed when the END request is sent
            if (done && rq.length == 0) {
                pthread_mutex_unlock(&rq_mut);
                return NULL;
            }
        }
        
        request = rq_dequeue(&rq);
        pthread_mutex_unlock(&rq_mut);

        switch (request->type) {
            case CHECK: {
                if (request->acc_id > 0 && request->acc_id <= num_accounts) {
                    pthread_mutex_lock(&acc_mut);
                    int balance = read_account(request->acc_id);
                    pthread_mutex_unlock(&acc_mut);

                    gettimeofday(&time, NULL);
                    fprintf(outfile, "%d BAL %d TIME %ld.%06ld %ld.%06ld\n", 
                        request->req_id, 
                        balance, 
                        request->start.tv_sec, 
                        request->start.tv_usec, 
                        time.tv_sec, 
                        time.tv_usec
                    );
                }
                break;
            }
            case TRANS: {
                bool *involved = trans_accounts(request->trans_list, num_accounts);
                bool success = true;
                int accounts[num_accounts];

                pthread_mutex_lock(&acc_mut);
                int i;
                for (i = 0; i < num_accounts; i++) {
                    if (involved[i]) {
                        accounts[i] = read_account(i + 1);
                    } else {
                        accounts[i] = 0;
                    }
                }

                for (i = 0; request->trans_list[i] != NULL; i++) {
                    Transaction *t = request->trans_list[i];
                    accounts[t->acc_id - 1] += t->amt;
                    gettimeofday(&time, NULL);

                    // Check for and handle ISF
                    if (accounts[t->acc_id - 1] < 0) {
                        fprintf(outfile, "%d ISF %d TIME %ld.%06ld %ld.%06ld\n",
                            request->req_id,
                            t->acc_id,
                            request->start.tv_sec,
                            request->start.tv_usec,
                            time.tv_sec,
                            time.tv_usec
                        );
                        success = false;
                        break;
                    }
                }
                pthread_mutex_unlock(&acc_mut);
                
                if (success) {
                    for (i = 0; i < num_accounts; i++) {
                        if (involved[i]) {
                            write_account(i + 1, accounts[i]);
                        }
                    }
                    gettimeofday(&time, NULL);
                    fprintf(outfile, "%d OK TIME %ld.%06ld %ld.%06ld\n",
                        request->req_id,
                        request->start.tv_sec,
                        request->start.tv_usec,
                        time.tv_sec,
                        time.tv_usec
                    );
                }
                
                for (i = 0; request->trans_list[i] != NULL; i++) {
                    free(request->trans_list[i]);
                }
                free(request->trans_list);
                free(involved);
                break;
            }
        }

        free(request);
    }

    return NULL;
}