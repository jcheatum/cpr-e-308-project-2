#ifndef _TRANSACTION_H
#define _TRANSACTION_H

#include <stdbool.h>
#include <stddef.h>

// Maximum number of transactions allowed in a TRANS request
#ifndef MAX_TRANS
#define MAX_TRANS 10
#endif

/**
 *  Contains the information for a change to a single account.
 */
typedef struct {
    // The ID of the account to modify
    int acc_id;

    // The amount to add to the account
    int amt;
} Transaction;

/**
 *  Parse a `TRANS` request into a null-terminated array of `Transaction` 
 *  objects.
 */
Transaction **parse_trans_list(char **request, size_t len);

/**
 *  Determine which account IDs are involved in a transaction. A given index in
 *  the returned array corresponds to account ID `index + 1`.
 */
bool *trans_accounts(Transaction **list, int num_accounts);

#endif