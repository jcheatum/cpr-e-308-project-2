#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "line.h"

char *fgetline(FILE *stream) {
    char *buf = calloc(GET_BUF_W, sizeof(char));
    if (buf == NULL) {
        perror("Error");
        exit(1);
    }
    size_t len = GET_BUF_W, i = 0;
    char c = 0;

    /* Append characters to `buf` until the end of the line, resizing `buf` as
     * necessary. */
    while (c != '\n') {
        c = fgetc(stream);
        buf[i++] = c;
        if (i == len) {
            buf = realloc(buf, (len *= 2) * sizeof(char));
        }
    }

    // remove trailing newline and make it a proper string
    buf[i - 1] = '\0'; 
    return buf;
}

char **split(char *line) {
    char **tokbuf = calloc(TOK_BUF_W, sizeof(char *));
    if (tokbuf == NULL) {
        perror("Error");
        exit(1);
    }
    size_t len = TOK_BUF_W, i = 0;

    char *tok = strtok(line, " ");
    while (tok != NULL) {
        tokbuf[i] = calloc(strlen(tok) + 1, sizeof(char));
        strcpy(tokbuf[i++], tok);
        tok = strtok(NULL, " ");
        if (i == len) {
            tokbuf = realloc(tokbuf, (len *= 2) * sizeof(char *));
        }
    }

    tokbuf[i] = NULL;
    return tokbuf;
}

size_t splitlen(char **list) {
    size_t i;
    for (i = 0; list[i] != NULL; i++);
    return i;
}