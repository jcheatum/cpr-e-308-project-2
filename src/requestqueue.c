#include <stdlib.h>

#include "requestqueue.h"

void rq_init(RequestQueue *rq) {
    rq->length = 0;
    rq->head = malloc(sizeof(struct node));
    rq->tail = malloc(sizeof(struct node));
    rq->head->next = rq->tail;
    rq->head->prev = NULL;
    rq->head->data = NULL;
    rq->tail->next = NULL;
    rq->tail->prev = rq->head;
}

void rq_enqueue(RequestQueue *rq, Request *r) {
    struct node *n = malloc(sizeof(struct node));
    n->data = r;
    n->next = rq->head->next;
    n->next->prev = n;
    n->prev = rq->head;
    rq->head->next = n;
    rq->length++;
}

Request *rq_dequeue(RequestQueue *rq) {
    struct node *n = rq->tail->prev;
    Request *r = n->data;
    rq->tail->prev = n->prev;
    n->prev->next = rq->tail;
    free(n);
    rq->length--;
    return r;
}

void rq_free(RequestQueue *rq) {
    struct node *n;
    for (n = rq->head->next; n != NULL; n = n->next) {
        free(n->prev);
    }
    free(rq->tail);
}