#include <stdlib.h>

#include "transaction.h"
#include "line.h"

Transaction **parse_trans_list(char **request, size_t len) {
    /* subtract 1 from the length of the request to ignore the 0th argument,
     * divide that by 2 since it's in account/amount pairs */
    Transaction **list = calloc(((len - 1) / 2) + 1, sizeof(Transaction *));

    size_t i, t_ind = 0;
    for (i = 1; request[i] != NULL; i += 2) {
        list[t_ind] = malloc(sizeof(Transaction));
        list[t_ind]->acc_id = atoi(request[i]);
        list[t_ind]->amt = atoi(request[i + 1]);
        t_ind++;
    }
    list[t_ind] = NULL;

    return list;
}

bool *trans_accounts(Transaction **list, int num_accounts) {
    bool *involved = calloc(num_accounts, sizeof(bool));
    int i;
    for (i = 0; list[i] != NULL; i++) {
        involved[list[i]->acc_id - 1] = true;
    }
    return involved;
}