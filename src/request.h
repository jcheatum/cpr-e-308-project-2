#ifndef _REQUEST_H
#define _REQUEST_H

#include <sys/time.h>

#include "transaction.h"

typedef enum {
    CHECK,
    TRANS
} RequestType;

/**
 *  A request parsed from a line of input.
 */
typedef struct {
    // The ID of the request
    int req_id;

    // The type of the request
    RequestType type;

    // The account ID of a CHECK request. Ignored if `type == TRANS`
    int acc_id;

    // The list of transactions in a TRANS request. Ignored if `type == CHECK`
    Transaction **trans_list;

    // The start time of the request.
    struct timeval start;
} Request;

#endif