#ifndef _REQUESTQUEUE_H
#define _REQUESTQUEUE_H

#include <stddef.h>

#include "request.h"

struct node {
    struct node *next;
    struct node *prev;

    Request *data;
};

/**
 *  A queue for holding requests waiting to be processed by a worker thread.
 */
typedef struct {
    struct node *head;
    struct node *tail;
    size_t length;
} RequestQueue;

/**
 *  Initialize a request queue. Should be called once before the queue is used.
 */
void rq_init(RequestQueue *rq);

/**
 *  Add a request to the queue.
 */
void rq_enqueue(RequestQueue *rq, Request *r);

/**
 *  Take the next request off the queue.
 */
Request *rq_dequeue(RequestQueue *rq);

/**
 *  Free all of the nodes in the queue.
 */
void rq_free(RequestQueue *rq);

#endif