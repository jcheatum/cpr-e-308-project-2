#ifndef _LINE_H
#define _LINE_H

#include <stdio.h>

/**
 *  Initial width of the buffer in `fgetline()`. The default value of 32 is
 *  arbitrary.
 *
 *  Can be redefined at compile time by passing `-DGET_BUF_W <number>` to gcc
 */
#ifndef GET_BUF_W
#define GET_BUF_W 32
#endif

/**
 *  Initial width of the buffer in `split()`. 
 */
#ifndef TOK_BUF_W
#define TOK_BUF_W GET_BUF_W/4
#endif

/**
 *  Read the next line of arbitrary length from `stream`.
 */
char *fgetline(FILE *stream);

/**
 *  Split `line` into a null-terminated list of space delimited tokens.
 */
char **split(char *line);

/**
 *  Get the length of a null-terminated list of strings, such as those returned
 *  from `split()`.
 */
size_t splitlen(char **list);

#endif